const cassandra = require('cassandra-driver');
const env = require('./env');


const client = new cassandra.Client({contactPoints: [env.DATABASE_HOST]});

const id = cassandra.types.Uuid.random();
var cassandra_db = {
    connect: function () {
        return client.connect()
            .then(function () {
                console.log('Connected to cluster with %d host(s): %j', client.hosts.length, client.hosts.keys());
                console.log('Keyspaces: %j', Object.keys(client.metadata.keyspaces));
                return 'Connection Successful'

            })
            .catch(function (err) {
                console.error('There was an error when connecting', err);
                return 'Connection Successfu'
            });
    },
    insert: function (app_id, actor_type, actor_id, verb, object, target, extra) {
        console.log(app_id)
        console.log(actor_type)
        console.log(actor_id)
        console.log(verb)
        console.log(object)
        console.log(target)
        console.log(extra)
        return client.connect()/*.then(
            function (value) {
                var query = "CREATE KEYSPACE IF NOT EXISTS collective WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '3' }";
                client.execute(query);
                return "done"
            }
        ).then(function (value) {
            var query = "CREATE TABLE IF NOT EXISTS collective.feed (id uuid, app_id text, actor_type text,  actor_id int, verb text, object text, target text, extra text, PRIMARY KEY(id,actor_id))";
            client.execute(query);
        })*/.then(function (value) {
            var query = 'INSERT INTO ' + env.KEYSPACE + '.feed (id,app_id, actor_type, actor_id, verb, object, target, extra) VALUES (?,?,?,?,?,?,?,?)';
            return client.execute(query, [id, app_id, actor_type, actor_id, verb, object, target, extra], {prepare: true});

        }).catch(function (err) {
            console.error('There was an error when connecting', err);
            return 'Connection Successfull'
        });


    },
    getSelfFeed: function (actor_id) {

        return client.connect().then(function (value) {
            var query = 'SELECT * FROM ' + env.KEYSPACE + '.feed where actor_id =?';
            return client.execute(query, [actor_id], {prepare: true}).then(function (value2) {
                console.log(value2)
                return value2.rows;
            })
        });
    },

}


exports.db = cassandra_db