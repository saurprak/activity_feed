const env = {
    DATABASE_HOST: '127.0.0.1',
    KEYSPACE: 'collective'
}

module.exports = env