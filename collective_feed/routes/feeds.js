var express = require('express');
var data = require('../db/cassandra_db')
var router = express.Router();

/* GET home page. */
router.get('/selfFeed/:actor_id', function (req, res, next) {
    actor_id = req.params.actor_id;
    data.db.getSelfFeed(actor_id).then(function (value) {
        res.json(value)
    })

});
router.post('/feed', function (req, res, next) {
    console.log(req.body);
    actor_type = req.body.actor_type;
    app_id = req.body.app_id;
    actor_id = parseInt(req.body.actor_id);
    verb = req.body.verb;
    object = req.body.object;
    target = req.body.target;
    extra = req.body.extra;
    data.db.insert(app_id, actor_type, actor_id, verb, object, target, extra).then(function (value) {
        console.log(value);
        res.json(value);
    })
});


module.exports = router;
