# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Add all the code for activity feed. 

### Cassandra Set up ###

* create keyspaces
* create table feed 
```
CREATE TABLE IF NOT EXISTS keyspace.feed (id uuid, app_id text, actor_type text,  actor_id int, verb text, object text, target text, extra text, PRIMARY KEY(id,actor_id))
```

* Check syntax for cqlsh to create table.
* create index for actor_id using cqlsh 

```
CREATE INDEX actor_id ON keyspace.feed(actor_id);
```

### How do I get set up? ###

* Go to collective feed
* npm install
* npm start
* update DB information in db/env.js
* API URLS
* Add Feed
```
* POST /feed

{
      "actor_type":"expression",
      "actor_id":4,
      "verb":"posted",
      "object":"image_url",
      "target":"@userID",
      "app_id":"1234",
      "extra":"extra information"
}
```

```
* GET /selfFeed/:id
```
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines
